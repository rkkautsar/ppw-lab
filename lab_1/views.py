from django.shortcuts import render
from datetime import datetime, date
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message
# Enter your name here
mhs_name = 'rk'  # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
# TODO Implement this, format (Year, Month, Date)
birth_date = date(1997, 6, 1)
npm = 1506688784  # TODO Implement this
# Create your views here.
response = {}


def index(request):
    # response = {'name': mhs_name, 'age': calculate_age(
    #     birth_date.year), 'npm': npm}
    response['message_form'] = Message_Form
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(name=response['name'], email=response['email'],
                      message=response['message'])
        message.save()
        html = 'form_result.html' 
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/')


def message_table(request):
    message = Message.objects.all() 
    response['message'] = message
    html = 'table.html'
    return render(request, html, response)
