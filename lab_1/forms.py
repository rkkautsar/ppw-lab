from django import forms


class Message_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email', }

    attr = {'class': 'form-control'}

    name = forms.CharField(label='Nama', required=False, max_length=27,widget=forms.TextInput(attrs=attr), empty_value="Anonymous")
    email = forms.EmailField(required=False, widget=forms.EmailInput(attrs=attr))
    message = forms.CharField(widget=forms.Textarea(attrs=attr), required=True)
